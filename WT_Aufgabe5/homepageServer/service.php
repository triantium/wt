<?php

/**
 * auf true setzen, um DEBUG-Info in das PHP-ErrorLog zu schreiben
 */
$debugToErrorLog = true;

function __autoload($class_name) {
    include $class_name . '.php';
}

/*
 * Services
 */

/**
 * GET person: liefert eine Personenbeschreibung
 */
function getPerson() {
    $person = Persistenz::getInstance()::ladePerson();
    //var_dump($person);
    echo $person;
    exit();
}

/**
 * PUT person <JSON-Personenbeschreibung>: speichert eine Personenbeschreibung
 */
function putPerson($person) {
    Persistenz::getInstance()->speicherePerson($person);
    exit();
}

/**
 * POST login <Benutzername/Passwort>
 * Prüft anhand der in der Datei benutzer.txt abgelegten Daten, ob Benutzername und Passwort korrekt sind.
 * Setzt einen entsprechenden Statuscode, 200: OK, 404: Fehler
 */
function postLogin($data) {
    $benutzer = Persistenz::getInstance()->ladeBenutzer();
    //var_dump($benutzer);
    $corBen = $benutzer["benutzername"];
    $corPw = $benutzer["passwort"];
    //var_dump($data);
    if ($data->benutzername === $corBen && $data->passwort === $corPw){
        http_response_code(200);
    }else{
        http_response_code(403);
    }
        exit();
}

/*
 * Service Dispatcher
 */
//var_dump($_SERVER["PATH_INFO"]);
//var_dump($_REQUEST);
//$_REQUEST['_url']

$url = $_SERVER["PATH_INFO"];
$requestType = $_SERVER['REQUEST_METHOD'];
$body = file_get_contents('php://input');
$jsonData = json_decode($body);



if ($GLOBALS["debugToErrorLog"]) {
    error_log("REST-Call: " . $requestType . ' ' . $url . ':' . $body);
}

if ($url === '/person') {
    if ($requestType === 'GET') {
        return getPerson();
    }
    if ($requestType === 'PUT') {
        return putPerson($jsonData);
    }
} else if ($url === '/login' && $requestType === 'POST') {
    return postLogin($jsonData);
} else {
    badRequest($requestType, $url, $body);
}

function badRequest($requestType, $url, $body) {
    http_response_code(400);
    if ($GLOBALS["debugToErrorLog"]) {
        error_log("bad request");
    }
    die('Ungültiger Request: ' . $requestType . ' ' . $url . ' ' . $body);
}

?>