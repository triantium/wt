<?php
session_start();
if (!isset($_SESSION['name'])) {

    $_SESSION['name'] = get_current_user();
    $_SESSION['ort'] = 'Nimmerland';
    $_SESSION['date'] = date('Y-m-d', time());
    $_SESSION['correctPW'] = '123';
    $_SESSION['imgPath'] = '/img/bart.jpeg';
}
$name = $_SESSION['name'];
$ort = $_SESSION['ort'];
$birthdate = $_SESSION['date'];
$correctPassword = $_SESSION['correctPW'];
$imagePath = $_SESSION['imgPath'];
?>
<!DOCTYPE html>
<html>
    <head>
        <title>SteckBrief</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <link rel="stylesheet" href="style/style.css">
    </head>
    <body>
        <!--
        <?php
        var_dump($_SESSION);
        ?>
        -->
        <div class="container">
            <h2>Willkommen auf der Homepage von <?php echo $name ?></h2>
            <div id="content">
                <nav id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    
                        <a class="nav-link" href="#steckbrief" data-toggle="tab">Steckbrief</a>
                  
                  
                        <a class="nav-link" href="#details" data-toggle="tab">mehr Details...</a>
                   
       
                        <a class="nav-link" href="#links" data-toggle="tab">meine Links</a>
                    
                </nav>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active" id="steckbrief">
                        <h3>Mein Steckbrief:</h3>
                        <div id="steckbriefPicture" class="float-left">
                            <img id="picture" class="steckbriefBild" alt="Fehler beim Laden des Bildes. Bitte gib einen g&uuml;ltigen Pfad ein!" src="<?php echo $imagePath; ?>">
                        </div>
                        <div id="steckbriefSpacer">&nbsp;</div>
                        <div id="steckbriefDaten">
                            <table>
                                <?php
                                echo '<tr><td>Name:</td><td>' . $name . '</td></tr>';
                                echo '<tr><td>Geburtstag:</td><td>' . $birthdate . '</td></tr>';
                                echo '<tr><td>Ort:</td><td>' . $ort . '</td></tr>';
                                echo '<tr><td>Bildpfad:</td><td>' . $imagePath . '</td></tr>';
                                echo '<tr><td>Passwort(zum Debuggen):</td><td>' . $correctPassword . '</td></tr>';
                                ?>

                            </table>
                            <form action="change.php">
                                <button class="btn btn-block"type="submit">Daten ändern</button>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="details">
                        <h3>Mehr &uuml;ber mich:</h3>
                        <p>...</p>
                    </div>
                    <div class="tab-pane" id="links">
                        <h3>Meine Linksammlung:</h3>
                        <p>...</p>
                    </div>
                </div>
            </div>
            <!-- content -->
        </div>
        <!-- container -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

    </body>
</html>