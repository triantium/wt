<?php
session_start();
if (isset($_POST['password']) && isset($_SESSION['correctPW'])) {
    if ($_SESSION['correctPW'] === $_POST['password']) {
        $_SESSION['loggedIn']=true;
        header('Location: change.php');
    } else {

        $error = 'falsches Passwort';
        $error = urlencode($error);
        $location = 'login.php?loginError=' . $error;
        header('Location: ' . $location);
    }
} else {
    
    $error = 'Passwort='.$_POST['password'].'Session'. $_SESSION['correctPW'];
    
    $error = urlencode($error);
    $location = 'login.php?loginError=' . $error;
    header('Location: ' . $location);
}
?>

