<?php
session_start();
if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] === FALSE) {

    header('Location: login.php');
}
if (isset($_GET['name'])&&isset($_GET['ort'])&&isset($_GET['date'])) {
    $_SESSION['name'] = $_GET['name'];
    $_SESSION['ort'] = $_GET['ort'];
    $_SESSION['date'] = $_GET['date'];
    header('Location: index.php');
} else if (isset($_SESSION['name'])) {
    $name = $_SESSION['name'];
    $ort = $_SESSION['ort'];
    $date = $_SESSION['date'];
} else {
    $name = '';
    $ort = '';
    $date = '';
}
?>
<!doctype html>
<html lang="en">
    <head>
        <title>Hello, world!</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <link rel="stylesheet" href="style/style.css">
    </head>
    <body>
        <div class="container">
            <form action="change.php" method="get">
                <div class="form-group">
                    <label  class="control-label">Name</label>
                    <input class="form-control" type="text" name="name" value="<?php if(isset($name))echo $name ?>" required="true"/>
                    <span class="help-block"></span>
                </div>
                <div class="form-group">
                    <label  class="control-label">Ort</label>
                    <input class="form-control" type="text" name="ort" value="<?php if(isset($ort))echo $ort ?>" required="true"/>
                    <span class="help-block"></span>
                </div>
                <div class="form-group">
                    <label  class="control-label">Datum</label>
                    <input class="form-control" type="date" name="date" value="<?php if(isset($date))echo $date?>" required="true"/>
                    <span class="help-block"></span>
                </div>
                <button type="submit" class="btn btn-success btn-block">senden</button>
                <button type="button" class="btn btn-danger btn-block" onclick="window.location = 'index.php'">abbrechen</button>
            </form>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    </body>
</html>
